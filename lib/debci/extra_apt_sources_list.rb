require 'yaml'
require 'debci/extra_apt_source'
module Debci
  class ExtraAptSourcesList
    def initialize(file)
      if File.exist?(file)
        @extra_apt_sources_list = YAML.safe_load(File.read(file))
        @extra_apt_sources_list.each do |key, value|
          @extra_apt_sources_list[key] = Debci::ExtraAptSource.new(value['entry'], value['allowed_users'], value['signing_key'])
        end
      else
        @extra_apt_sources_list = {}
      end
    end

    def find(name)
      @extra_apt_sources_list[name]
    end

    def allowed_extra_apt_sources(user)
      @extra_apt_sources_list.select { |_key, value| value.allowed?(user) }.keys
    end
  end
end
