require 'thor'
require 'debci/html'
require 'debci/package'

module Debci
  class HTML
    class CLI < Thor
      desc 'update', "Updates global HTML pages"
      def update
        Debci::HTML.update
      end

      desc 'update-package PACKAGE', 'Updates HTML for a given package'
      def update_package(pkg)
        package = Debci::Package.where(name: pkg).first
        Debci::HTML.update_package(package)
      end

      def self.exit_on_failure?
        true
      end
    end
  end
end
