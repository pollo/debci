#!/bin/sh

parallel=parallel
if command -v parallel.moreutils >/dev/null; then
  parallel=parallel.moreutils
fi
$parallel ./bin/debci worker --arch -- $(./bin/debci config --values-only arch_list)
